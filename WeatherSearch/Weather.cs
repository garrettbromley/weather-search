﻿using System;

namespace WeatherSearch
{
    class Weather
    {
        public string city { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string weather { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
        public double temperature { get; set; }
        public double humidity { get; set; }
        public double lowTemp { get; set; }
        public double highTemp { get; set; }
        public double windSpeed { get; set; }
        public double windDirection { get; set; }
        public int cloudCoverage { get; set; }
        public DateTime sunrise { get; set; }
        public DateTime sunset { get; set; }

        public Weather(string city, double latitude, double longitude, string weather, string description, string icon, double temperature, double humidity, double lowTemp, double highTemp, double windSpeed, double windDirection, int cloudCoverage, int sunrise, int sunset)
        {
            this.city = city;
            this.latitude = latitude;
            this.longitude = longitude;
            this.weather = weather;
            this.description = description;
            this.icon = icon;
            this.temperature = temperature;
            this.humidity = humidity;
            this.lowTemp = lowTemp;
            this.highTemp = highTemp;
            this.windSpeed = windSpeed;
            this.windDirection = windDirection;
            this.cloudCoverage = cloudCoverage;
            this.sunrise = DateTimeOffset.FromUnixTimeSeconds(sunrise).DateTime.ToLocalTime();
            this.sunset = DateTimeOffset.FromUnixTimeSeconds(sunset).DateTime.ToLocalTime();
        }
    }
}

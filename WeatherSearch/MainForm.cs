/*
    Project: Weather Search Program
    Written By: 
        - Garrett Bromley
        - Ted Wallick
        - Ben Graul
    Description:
        Allows a user to enter any zip code and query and 
        display the weather data for that zip code.
*/

using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace WeatherSearch
{
    public partial class MainForm : Form
    {
        private string weatherKey = "b9eb0ac01c67bfbe4e3426849c20d396";
        private string ipKey = "d2f7a464f6eb2d";
        private Weather weather;
        private IpInfo ipInfo;

        private System.Drawing.Image imageOriginal;

        public MainForm() // Main form.
        {
            InitializeComponent();
            this.AcceptButton = this.searchButton;
        }

        private void SearchButton_Click(object sender, EventArgs e) // Queries the information from the weather API.
        {
            // Try to parse the entry.
            bool zipTrue = int.TryParse(zipCodeBox.Text, out int zipCodeTest);

            // If the length matches the format and it can be parsed
            // Run the query.
            if (zipCodeBox.Text.Length == 5 && zipTrue)
            {
                try
                {
                    weather = API.QueryWeather(zipCodeBox.Text, weatherKey);
                    if (weather is null)
                    {
                        MessageBox.Show("Unable to find weather data for " + zipCodeBox.Text + ".", "Not Found Error");
                        zipCodeBox.Clear();
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(zipCodeBox.Text + " is not a valid zip code.", "Format Error");
                // Get their current location and clear the box.
                ipInfo = API.QueryLocation(ipKey);
                weather = API.QueryWeather(ipInfo.Postal, weatherKey);
                zipCodeBox.Clear();
            }

            // Update the display.
            UpdateDisplay();
        }

        private void UpdateDisplay() // Updates the display.
        {
            cityLabel.Text = weather.city;
            tempLabel.Text = Math.Round(weather.temperature).ToString() + "\u00B0";
            lowTempLabel.Text = weather.lowTemp.ToString() + "\u00B0";
            highTempLabel.Text = weather.highTemp.ToString() + "\u00B0";
            humidityValueLabel.Text = String.Format("{0}%", weather.humidity);

            // Set the current time based labels.
            timeLabel.Text = DateTime.Now.ToString("h:mm tt");

            double totalMinutes;
            double minutesTaken;
            int riseDiff = 0;
            int setDiff = 0;

            if(DateTime.Now.Day != weather.sunrise.Day)
            {
                riseDiff = -1;
            }

            if (DateTime.Now.Day != weather.sunset.Day)
            {
                setDiff = -1;
            }

            bool wxSrDn = weather.sunrise.AddDays(riseDiff) <= DateTime.Now;
            bool wxSsDn = DateTime.Now <= weather.sunset.AddDays(setDiff);

            if ((wxSrDn) && (wxSsDn))
            {   // Daytime
                
                // Set sun on left and moon on right.
                sunrisePicture.Image = Properties.Resources.sunny;
                sunsetPicture.Image = Properties.Resources.moon;
                
                // Set sunrise and sunset values.
                sunriseTimeLabel.Text = weather.sunrise.ToString("h:mm tt");
                sunsetTimeLabel.Text = weather.sunset.ToString("h:mm tt");
                
                // Calculate total minutes between sunrise and sunset.
                totalMinutes = weather.sunset.AddDays(setDiff).Subtract(weather.sunrise.AddDays(riseDiff)).TotalMinutes;
                
                // Calculate total minutes since sunrise.
                minutesTaken = DateTime.Now.Subtract(weather.sunrise.AddDays(riseDiff)).TotalMinutes;
            }
            else
            {   // Nighttime
                
                // Swap sun and moon pics
                sunrisePicture.Image = Properties.Resources.moon;
                sunsetPicture.Image = Properties.Resources.sunny;
                
                // Reverse sunrise and sunset values for the evening.
                sunriseTimeLabel.Text = weather.sunset.ToString("h:mm tt");
                sunsetTimeLabel.Text = weather.sunrise.ToString("h:mm tt");

                // Calculate total minutes between sunset and sunrise.
                totalMinutes = (weather.sunrise.AddDays(riseDiff).Subtract(weather.sunset.AddDays(setDiff)).TotalMinutes);

                // Calculate minutes since sunset.
                minutesTaken = DateTime.Now.Subtract(weather.sunset.AddDays(setDiff)).TotalMinutes;

                totalMinutes = Math.Abs(totalMinutes);
                minutesTaken = Math.Abs(minutesTaken);

            }

            // Set the progress bar.
            dayProgressBar.Maximum = Convert.ToInt32(totalMinutes);
            dayProgressBar.Value = Convert.ToInt32(minutesTaken);

            windSpeedLabel.Text = String.Format("{0} mph", weather.windSpeed);

            // Start Wind direction out at 0 degree.
            this.windDirectionPicture.Image = imageOriginal;
            //this.windDirectionPicture.Image = Utilities.RotateImage(this.windDirectionPicture.Image, Convert.ToSingle(180));

            // Display the wind direction.
            this.windDirectionPicture.Image = Utilities.RotateImage(this.windDirectionPicture.Image, Convert.ToSingle(weather.windDirection));

            // Display contents of data for the additional fields we need to enhance.
            TextInfo cultInfo = new CultureInfo("en-US", false).TextInfo;
            curDescriptionLabel.Text = cultInfo.ToTitleCase(weather.description);
            curWeatherLabel.Text = weather.weather;
            coordinatesLabel.Text = Utilities.FormatLongLat(weather.longitude, weather.latitude);

            // Display the Current Weather Picture.
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(String.Format("https://openweathermap.org/img/w/{0}.png", weather.icon));
            MemoryStream ms = new MemoryStream(bytes);
            curWeatherPicture.Image = Image.FromStream(ms);
        }

        private void MainForm_Load(object sender, EventArgs e) // Loads the main form.
        {
            imageOriginal = this.windDirectionPicture.Image;

            ipInfo = API.QueryLocation(ipKey);
            weather = API.QueryWeather(ipInfo.Postal, weatherKey);
            UpdateDisplay();
        }
    }
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;

namespace WeatherSearch
{
    class API
    {
        public static Weather QueryWeather(string input, string authorizationKey) // Queries entered Zip code.
        {
            // Setup the request.
            var client = new RestClient(String.Format("https://api.openweathermap.org/data/2.5/weather?zip={0},us&appid={1}&units=imperial", input, authorizationKey));
            var request = new RestRequest(Method.GET);

            // Add the headers.
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");

            // Execute the request.
            IRestResponse response = client.Execute(request);
            JObject jObject = JObject.Parse(response.Content);

            // Parse the queried information into a weather object.
            try
            {
                return new Weather(
                        jObject["name"].ToString(),
                        Convert.ToDouble(jObject["coord"]["lat"].ToString()),
                        Convert.ToDouble(jObject["coord"]["lon"].ToString()),
                        jObject["weather"][0]["main"].ToString(),
                        jObject["weather"][0]["description"].ToString(),
                        jObject["weather"][0]["icon"].ToString(),
                        Convert.ToDouble(jObject["main"]["temp"].ToString()),
                        Convert.ToDouble(jObject["main"]["humidity"].ToString()),
                        Convert.ToDouble(jObject["main"]["temp_min"].ToString()),
                        Convert.ToDouble(jObject["main"]["temp_max"].ToString()),
                        Convert.ToDouble(jObject["wind"]["speed"].ToString()),
                        Convert.ToDouble(jObject["wind"]["deg"].ToString()),
                        Convert.ToInt32(jObject["clouds"]["all"].ToString()),
                        Convert.ToInt32(jObject["sys"]["sunrise"].ToString()),
                        Convert.ToInt32(jObject["sys"]["sunset"].ToString())
                    );
            }
            catch (Exception)
            {

                return null;
            }
        }

        public static IpInfo QueryLocation(string authorizationKey) // Queries user location by IP address for initial query results.
        {
            IpInfo ipInfo = new IpInfo();
            try
            {
                // Setup the request.
                var client = new RestClient(String.Format("https://ipinfo.io?token={0}", authorizationKey));
                var request = new RestRequest(Method.GET);

                // Add the headers.
                request.AddHeader("content-type", "application/json");
                request.AddHeader("accept", "application/json");

                // Execute the request.
                IRestResponse response = client.Execute(request);
                ipInfo = JsonConvert.DeserializeObject<IpInfo>(response.Content);
            }
            catch (Exception)
            {
                ipInfo = null;
            }

            return ipInfo;
        }
    }
}

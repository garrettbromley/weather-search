﻿using System;
using System.Drawing;

namespace WeatherSearch
{
    class Utilities
    {        
        /// <summary>
        /// Creates a new Image containing the same image only rotated.
        /// </summary>
        /// <param name=""image"">The <see cref=""System.Drawing.Image"/"> to rotate
        /// <param name=""offset"">The position to rotate from.
        /// <param name=""angle"">The amount to rotate the image, clockwise, in degrees.
        /// <returns>A new <see cref=""System.Drawing.Bitmap"/"> of the same size rotated.</see>
        /// <exception cref=""System.ArgumentNullException"">Thrown if <see cref=""image"/"> 
        /// is null.</see>
        public static Bitmap RotateImage(Image image, float angle)
        {
            // Wind direction offset.
            angle += 180;

            Bitmap rotatedImage = new Bitmap(image.Width, image.Height);
            using (Graphics g = Graphics.FromImage(rotatedImage))
            {
                // Set the rotation point to the center in the matrix.
                g.TranslateTransform(image.Width / 2, image.Height / 2);
                // Rotate.
                g.RotateTransform(angle);
                // Restore rotation point in the matrix.
                g.TranslateTransform(-image.Width / 2, -image.Height / 2);
                // Draw the image on the bitmap.
                g.DrawImage(image, new Point(0, 0));
            }

            return rotatedImage;
        }

        // Turns the weather based longitude and latitude to a formatted
        // String for use in the application.
        public static string FormatLongLat(double longitude, double latitude)
        {
            // Initialize the starting strings
            string longStr = String.Format("{0}° ", Math.Abs(longitude));
            string latStr = String.Format("{0}° ", Math.Abs(latitude));

            // Add the proper abbreviation for longitude.
            if (longitude >= 0)
            {
                longStr += "N";
            } else
            {
                longStr += "S";
            }

            // Add the proper abbreviation for latitude.
            if (latitude >= 0)
            {
                latStr += "E";
            } else
            {
                latStr += "W";
            }

            // Put them together.
            return longStr + ", " + latStr;
        }

    }
}

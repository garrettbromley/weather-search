﻿namespace WeatherSearch
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tempLabel = new System.Windows.Forms.Label();
            this.cityLabel = new System.Windows.Forms.Label();
            this.coordinatesLabel = new System.Windows.Forms.Label();
            this.sunsetPicture = new System.Windows.Forms.PictureBox();
            this.sunrisePicture = new System.Windows.Forms.PictureBox();
            this.dayProgressBar = new System.Windows.Forms.ProgressBar();
            this.timeLabel = new System.Windows.Forms.Label();
            this.lowLabel = new System.Windows.Forms.Label();
            this.highLabel = new System.Windows.Forms.Label();
            this.lowTempLabel = new System.Windows.Forms.Label();
            this.highTempLabel = new System.Windows.Forms.Label();
            this.curWeatherPicture = new System.Windows.Forms.PictureBox();
            this.curWeatherLabel = new System.Windows.Forms.Label();
            this.curDescriptionLabel = new System.Windows.Forms.Label();
            this.sunriseTimeLabel = new System.Windows.Forms.Label();
            this.sunsetTimeLabel = new System.Windows.Forms.Label();
            this.northLabel = new System.Windows.Forms.Label();
            this.southLabel = new System.Windows.Forms.Label();
            this.westLabel = new System.Windows.Forms.Label();
            this.eastLabel = new System.Windows.Forms.Label();
            this.windDirectionPicture = new System.Windows.Forms.PictureBox();
            this.windSpeedLabel = new System.Windows.Forms.Label();
            this.humidityValueLabel = new System.Windows.Forms.Label();
            this.humidityLabel = new System.Windows.Forms.Label();
            this.zipCodeBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.FLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.sunsetPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sunrisePicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.curWeatherPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windDirectionPicture)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tempLabel
            // 
            this.tempLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tempLabel.ForeColor = System.Drawing.Color.White;
            this.tempLabel.Location = new System.Drawing.Point(20, 58);
            this.tempLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tempLabel.Name = "tempLabel";
            this.tempLabel.Size = new System.Drawing.Size(161, 83);
            this.tempLabel.TabIndex = 0;
            this.tempLabel.Text = "--°";
            this.tempLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cityLabel
            // 
            this.cityLabel.AutoSize = true;
            this.cityLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cityLabel.ForeColor = System.Drawing.Color.White;
            this.cityLabel.Location = new System.Drawing.Point(-3, 35);
            this.cityLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.cityLabel.Name = "cityLabel";
            this.cityLabel.Size = new System.Drawing.Size(91, 83);
            this.cityLabel.TabIndex = 1;
            this.cityLabel.Text = "--";
            this.cityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // coordinatesLabel
            // 
            this.coordinatesLabel.AutoSize = true;
            this.coordinatesLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coordinatesLabel.ForeColor = System.Drawing.Color.White;
            this.coordinatesLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.coordinatesLabel.Location = new System.Drawing.Point(316, 9);
            this.coordinatesLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.coordinatesLabel.Name = "coordinatesLabel";
            this.coordinatesLabel.Size = new System.Drawing.Size(132, 20);
            this.coordinatesLabel.TabIndex = 2;
            this.coordinatesLabel.Text = "(00.00 E, 00.00 S)";
            // 
            // sunsetPicture
            // 
            this.sunsetPicture.Image = global::WeatherSearch.Properties.Resources.moon;
            this.sunsetPicture.Location = new System.Drawing.Point(336, 409);
            this.sunsetPicture.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.sunsetPicture.Name = "sunsetPicture";
            this.sunsetPicture.Size = new System.Drawing.Size(68, 52);
            this.sunsetPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.sunsetPicture.TabIndex = 3;
            this.sunsetPicture.TabStop = false;
            // 
            // sunrisePicture
            // 
            this.sunrisePicture.Image = global::WeatherSearch.Properties.Resources.sunny;
            this.sunrisePicture.Location = new System.Drawing.Point(37, 409);
            this.sunrisePicture.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.sunrisePicture.Name = "sunrisePicture";
            this.sunrisePicture.Size = new System.Drawing.Size(68, 52);
            this.sunrisePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.sunrisePicture.TabIndex = 4;
            this.sunrisePicture.TabStop = false;
            // 
            // dayProgressBar
            // 
            this.dayProgressBar.Location = new System.Drawing.Point(108, 431);
            this.dayProgressBar.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.dayProgressBar.Name = "dayProgressBar";
            this.dayProgressBar.Size = new System.Drawing.Size(226, 12);
            this.dayProgressBar.TabIndex = 5;
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.ForeColor = System.Drawing.Color.White;
            this.timeLabel.Location = new System.Drawing.Point(186, 446);
            this.timeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(68, 20);
            this.timeLabel.TabIndex = 6;
            this.timeLabel.Text = "0:00 PM";
            // 
            // lowLabel
            // 
            this.lowLabel.AutoSize = true;
            this.lowLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowLabel.ForeColor = System.Drawing.Color.White;
            this.lowLabel.Location = new System.Drawing.Point(211, 81);
            this.lowLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lowLabel.Name = "lowLabel";
            this.lowLabel.Size = new System.Drawing.Size(43, 19);
            this.lowLabel.TabIndex = 7;
            this.lowLabel.Text = "LOW";
            // 
            // highLabel
            // 
            this.highLabel.AutoSize = true;
            this.highLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.highLabel.ForeColor = System.Drawing.Color.White;
            this.highLabel.Location = new System.Drawing.Point(211, 150);
            this.highLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.highLabel.Name = "highLabel";
            this.highLabel.Size = new System.Drawing.Size(49, 19);
            this.highLabel.TabIndex = 8;
            this.highLabel.Text = "HIGH";
            // 
            // lowTempLabel
            // 
            this.lowTempLabel.AutoSize = true;
            this.lowTempLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 22.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowTempLabel.ForeColor = System.Drawing.Color.White;
            this.lowTempLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lowTempLabel.Location = new System.Drawing.Point(207, 37);
            this.lowTempLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lowTempLabel.Name = "lowTempLabel";
            this.lowTempLabel.Size = new System.Drawing.Size(55, 39);
            this.lowTempLabel.TabIndex = 9;
            this.lowTempLabel.Text = "--°";
            // 
            // highTempLabel
            // 
            this.highTempLabel.AutoSize = true;
            this.highTempLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 22.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.highTempLabel.ForeColor = System.Drawing.Color.White;
            this.highTempLabel.Location = new System.Drawing.Point(207, 106);
            this.highTempLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.highTempLabel.Name = "highTempLabel";
            this.highTempLabel.Size = new System.Drawing.Size(55, 39);
            this.highTempLabel.TabIndex = 10;
            this.highTempLabel.Text = "--°";
            // 
            // curWeatherPicture
            // 
            this.curWeatherPicture.Image = global::WeatherSearch.Properties.Resources.partly_cloudy;
            this.curWeatherPicture.Location = new System.Drawing.Point(158, 320);
            this.curWeatherPicture.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.curWeatherPicture.Name = "curWeatherPicture";
            this.curWeatherPicture.Size = new System.Drawing.Size(120, 96);
            this.curWeatherPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.curWeatherPicture.TabIndex = 11;
            this.curWeatherPicture.TabStop = false;
            // 
            // curWeatherLabel
            // 
            this.curWeatherLabel.AutoSize = true;
            this.curWeatherLabel.Location = new System.Drawing.Point(293, 347);
            this.curWeatherLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.curWeatherLabel.Name = "curWeatherLabel";
            this.curWeatherLabel.Size = new System.Drawing.Size(67, 20);
            this.curWeatherLabel.TabIndex = 12;
            this.curWeatherLabel.Text = "(Cloudy)";
            // 
            // curDescriptionLabel
            // 
            this.curDescriptionLabel.AutoSize = true;
            this.curDescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.curDescriptionLabel.ForeColor = System.Drawing.Color.White;
            this.curDescriptionLabel.Location = new System.Drawing.Point(292, 373);
            this.curDescriptionLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.curDescriptionLabel.Name = "curDescriptionLabel";
            this.curDescriptionLabel.Size = new System.Drawing.Size(134, 20);
            this.curDescriptionLabel.TabIndex = 13;
            this.curDescriptionLabel.Text = "(Overcast Cloudy)";
            // 
            // sunriseTimeLabel
            // 
            this.sunriseTimeLabel.AutoSize = true;
            this.sunriseTimeLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sunriseTimeLabel.ForeColor = System.Drawing.Color.White;
            this.sunriseTimeLabel.Location = new System.Drawing.Point(33, 463);
            this.sunriseTimeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sunriseTimeLabel.Name = "sunriseTimeLabel";
            this.sunriseTimeLabel.Size = new System.Drawing.Size(70, 20);
            this.sunriseTimeLabel.TabIndex = 14;
            this.sunriseTimeLabel.Text = "0:00 AM";
            // 
            // sunsetTimeLabel
            // 
            this.sunsetTimeLabel.AutoSize = true;
            this.sunsetTimeLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sunsetTimeLabel.ForeColor = System.Drawing.Color.White;
            this.sunsetTimeLabel.Location = new System.Drawing.Point(332, 463);
            this.sunsetTimeLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.sunsetTimeLabel.Name = "sunsetTimeLabel";
            this.sunsetTimeLabel.Size = new System.Drawing.Size(68, 20);
            this.sunsetTimeLabel.TabIndex = 15;
            this.sunsetTimeLabel.Text = "0:00 PM";
            // 
            // northLabel
            // 
            this.northLabel.AutoSize = true;
            this.northLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.northLabel.ForeColor = System.Drawing.Color.White;
            this.northLabel.Location = new System.Drawing.Point(52, 37);
            this.northLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.northLabel.Name = "northLabel";
            this.northLabel.Size = new System.Drawing.Size(21, 20);
            this.northLabel.TabIndex = 16;
            this.northLabel.Text = "N";
            // 
            // southLabel
            // 
            this.southLabel.AutoSize = true;
            this.southLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.southLabel.ForeColor = System.Drawing.Color.White;
            this.southLabel.Location = new System.Drawing.Point(52, 105);
            this.southLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.southLabel.Name = "southLabel";
            this.southLabel.Size = new System.Drawing.Size(18, 20);
            this.southLabel.TabIndex = 17;
            this.southLabel.Text = "S";
            // 
            // westLabel
            // 
            this.westLabel.AutoSize = true;
            this.westLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.westLabel.ForeColor = System.Drawing.Color.White;
            this.westLabel.Location = new System.Drawing.Point(12, 72);
            this.westLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.westLabel.Name = "westLabel";
            this.westLabel.Size = new System.Drawing.Size(24, 20);
            this.westLabel.TabIndex = 18;
            this.westLabel.Text = "W";
            // 
            // eastLabel
            // 
            this.eastLabel.AutoSize = true;
            this.eastLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eastLabel.ForeColor = System.Drawing.Color.White;
            this.eastLabel.Location = new System.Drawing.Point(91, 72);
            this.eastLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.eastLabel.Name = "eastLabel";
            this.eastLabel.Size = new System.Drawing.Size(17, 20);
            this.eastLabel.TabIndex = 19;
            this.eastLabel.Text = "E";
            // 
            // windDirectionPicture
            // 
            this.windDirectionPicture.BackColor = System.Drawing.Color.CornflowerBlue;
            this.windDirectionPicture.Image = ((System.Drawing.Image)(resources.GetObject("windDirectionPicture.Image")));
            this.windDirectionPicture.Location = new System.Drawing.Point(39, 59);
            this.windDirectionPicture.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.windDirectionPicture.Name = "windDirectionPicture";
            this.windDirectionPicture.Size = new System.Drawing.Size(45, 45);
            this.windDirectionPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windDirectionPicture.TabIndex = 20;
            this.windDirectionPicture.TabStop = false;
            // 
            // windSpeedLabel
            // 
            this.windSpeedLabel.AutoSize = true;
            this.windSpeedLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.windSpeedLabel.ForeColor = System.Drawing.Color.White;
            this.windSpeedLabel.Location = new System.Drawing.Point(25, 149);
            this.windSpeedLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.windSpeedLabel.Name = "windSpeedLabel";
            this.windSpeedLabel.Size = new System.Drawing.Size(56, 20);
            this.windSpeedLabel.TabIndex = 21;
            this.windSpeedLabel.Text = "-- mph";
            // 
            // humidityValueLabel
            // 
            this.humidityValueLabel.AutoSize = true;
            this.humidityValueLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humidityValueLabel.ForeColor = System.Drawing.Color.White;
            this.humidityValueLabel.Location = new System.Drawing.Point(36, 352);
            this.humidityValueLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.humidityValueLabel.Name = "humidityValueLabel";
            this.humidityValueLabel.Size = new System.Drawing.Size(81, 46);
            this.humidityValueLabel.TabIndex = 23;
            this.humidityValueLabel.Text = "--%";
            this.humidityValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // humidityLabel
            // 
            this.humidityLabel.AutoSize = true;
            this.humidityLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humidityLabel.ForeColor = System.Drawing.Color.White;
            this.humidityLabel.Location = new System.Drawing.Point(40, 333);
            this.humidityLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.humidityLabel.Name = "humidityLabel";
            this.humidityLabel.Size = new System.Drawing.Size(85, 19);
            this.humidityLabel.TabIndex = 22;
            this.humidityLabel.Text = "HUMIDITY";
            this.humidityLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // zipCodeBox
            // 
            this.zipCodeBox.AcceptsReturn = true;
            this.zipCodeBox.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zipCodeBox.Location = new System.Drawing.Point(9, 6);
            this.zipCodeBox.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.zipCodeBox.Name = "zipCodeBox";
            this.zipCodeBox.Size = new System.Drawing.Size(112, 26);
            this.zipCodeBox.TabIndex = 24;
            // 
            // searchButton
            // 
            this.searchButton.AutoSize = true;
            this.searchButton.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            this.searchButton.ForeColor = System.Drawing.Color.Black;
            this.searchButton.Location = new System.Drawing.Point(121, 5);
            this.searchButton.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(86, 27);
            this.searchButton.TabIndex = 25;
            this.searchButton.Text = "SEARCH";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.FLabel);
            this.groupBox1.Controls.Add(this.tempLabel);
            this.groupBox1.Controls.Add(this.lowTempLabel);
            this.groupBox1.Controls.Add(this.lowLabel);
            this.groupBox1.Controls.Add(this.highLabel);
            this.groupBox1.Controls.Add(this.highTempLabel);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(6, 130);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(316, 187);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TEMPERATURE";
            // 
            // FLabel
            // 
            this.FLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FLabel.Location = new System.Drawing.Point(158, 69);
            this.FLabel.Name = "FLabel";
            this.FLabel.Size = new System.Drawing.Size(29, 28);
            this.FLabel.TabIndex = 30;
            this.FLabel.Text = "F";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.windDirectionPicture);
            this.groupBox2.Controls.Add(this.northLabel);
            this.groupBox2.Controls.Add(this.southLabel);
            this.groupBox2.Controls.Add(this.westLabel);
            this.groupBox2.Controls.Add(this.eastLabel);
            this.groupBox2.Controls.Add(this.windSpeedLabel);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(324, 130);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(121, 187);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "WIND";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ClientSize = new System.Drawing.Size(451, 478);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.zipCodeBox);
            this.Controls.Add(this.humidityValueLabel);
            this.Controls.Add(this.humidityLabel);
            this.Controls.Add(this.sunsetTimeLabel);
            this.Controls.Add(this.sunriseTimeLabel);
            this.Controls.Add(this.curDescriptionLabel);
            this.Controls.Add(this.curWeatherLabel);
            this.Controls.Add(this.curWeatherPicture);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.dayProgressBar);
            this.Controls.Add(this.sunrisePicture);
            this.Controls.Add(this.sunsetPicture);
            this.Controls.Add(this.coordinatesLabel);
            this.Controls.Add(this.cityLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Weather Searcher";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sunsetPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sunrisePicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.curWeatherPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windDirectionPicture)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label tempLabel;
        private System.Windows.Forms.Label cityLabel;
        private System.Windows.Forms.Label coordinatesLabel;
        private System.Windows.Forms.PictureBox sunsetPicture;
        private System.Windows.Forms.PictureBox sunrisePicture;
        private System.Windows.Forms.ProgressBar dayProgressBar;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label lowLabel;
        private System.Windows.Forms.Label highLabel;
        private System.Windows.Forms.Label lowTempLabel;
        private System.Windows.Forms.Label highTempLabel;
        private System.Windows.Forms.PictureBox curWeatherPicture;
        private System.Windows.Forms.Label curWeatherLabel;
        private System.Windows.Forms.Label curDescriptionLabel;
        private System.Windows.Forms.Label sunriseTimeLabel;
        private System.Windows.Forms.Label sunsetTimeLabel;
        private System.Windows.Forms.Label northLabel;
        private System.Windows.Forms.Label southLabel;
        private System.Windows.Forms.Label westLabel;
        private System.Windows.Forms.Label eastLabel;
        private System.Windows.Forms.PictureBox windDirectionPicture;
        private System.Windows.Forms.Label windSpeedLabel;
        private System.Windows.Forms.Label humidityValueLabel;
        private System.Windows.Forms.Label humidityLabel;
        private System.Windows.Forms.TextBox zipCodeBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label FLabel;
    }
}

